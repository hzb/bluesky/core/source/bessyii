import warnings
import collections
import ast
import re
from IPython.core.magic import register_line_magic
from collections import OrderedDict

from IPython import get_ipython
user_ns = get_ipython().user_ns


class PlotSelect:
    """
    A class to handle the selection and management of detectors.

    Attributes:
        detectors_dict (OrderedDict): Dictionary of detectors with their names as keys.
        RE (object): The bluesky RUnEngine
        plot_detector (list): List of currently selected detectors.
    """

    def __init__(self, RE, user_ns, detectors) -> None:
        """
        Initializes the PlotSelect object with detectors and updates the user namespace.

        Args:
            RE (object): An object with a 'md' attribute for metadata storage.
            user_ns (dict): The user namespace to update with plot detectors.
            *detectors: Variable length argument list of detector objects.
        """
        # Create an ordered dictionary of detectors
        self.detectors_dict = OrderedDict((detector.name, detector) for detector in detectors)
        self.RE = RE
        self.user_ns = user_ns
        self.plot_detector = []

        try:
            det_string = ''
            # Append detectors from RE.md to plot_detector and create a string representation
            for det in self.RE.md["plot_detector"]:
                self.plot_detector.append(self.detectors_dict[det])
                det_string += f"'{det}',"
            # Update user namespace with plot detectors
            self.update_user_ns()
        except (KeyError, TypeError):
            # Handle the case where RE.md["plot_detector"] is not set or invalid
            self.plot_detector = [detectors[0]]
            self.RE.md["plot_detector"] = [detectors[0].name]
            self.user_ns['plot_detectors'] = []

    def update_user_ns(self):
        """
        Updates the user namespace with the current plot detectors.
        """
        self.user_ns['plot_detectors'] = self.plot_detector

    def get_user_choices(self):
        """
        Prompts the user to select detectors from the available list.

        Returns:
            list: A list of selected detector objects.
        """
        print("Available detectors:")
        for index, det in enumerate(self.detectors_dict.keys(), 1):
            print(f"{index}. {det}")

        choices = input("Select detectors by one or more numbers (e.g., '1', '1 2', '1,2'): ")

        # Normalize input: remove commas, split by spaces, and convert to integers
        choices = [int(choice.strip()) - 1 for choice in choices.replace(',', ' ').split()]

        # Validate choices and return valid detectors
        keys_list = list(self.detectors_dict.keys())
        return [self.detectors_dict[keys_list[choice]] for choice in choices if 0 <= choice < len(keys_list)]

    def plotselect(self, *args):
        """
        Sets the selected detectors for plotting.

        Args:
            *args (int): Detector indices to select.

        Raises:
            ValueError: If the detectors argument is not an int or list.
        """
        if args:
            # Convert args to a list of indices
            detectors = [int(arg) for arg in args]

            # Validate choices and return valid detectors
            keys_list = list(self.detectors_dict.keys())
            choices = [self.detectors_dict[keys_list[detector - 1]] for detector in detectors if 0 <= detector - 1 < len(keys_list)]
        else:
            print(f"Current detector: {self.RE.md['plot_detector']}")
            print('Press enter to exit')

            choices = self.get_user_choices()

        if choices:
            self.plot_detector = choices
            self.RE.md["plot_detector"] = [detector.name for detector in choices]
            det_string = ''
            # Create a string representation of selected detectors
            for det in self.RE.md["plot_detector"]:
                det_string += f"'{det}',"
            # Update user namespace with plot detectors
            self.update_user_ns()
            print(f"Detectors set to: {self.RE.md['plot_detector']}")
        else:
            print("Invalid choices, no changes made.")






def create_magics_from_list(names_list:list):
    """Create and register magics starting from a list of names
    
    The names will be included in a command like this:
    RE(names_list[i] *args)
    
    See: make_command
    Args:
        names_list (list): list of plans

    Returns:
        eval(RE(names_list[i] *args))
        
    Requirements:
        before calling this function, define aliases for the plans. 
        For instance, for count define:
            count_plan  = count 
            imported_objects = get_imported_objects('/opt/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/plans.py')
            plan_names = [obj for obj in imported_objects if not obj.startswith('_')]
            create_magics_from_list(plan_names)
        
        In beamlinetools.BEAMLINE_CONFIG.__init__.py, after the import of all the functions:
            imported_objects = get_imported_objects('/opt/bluesky/beamlinetools/beamlinetools/BEAMLINE_CONFIG/plans.py')
            plan_names = [obj for obj in imported_objects if not obj.startswith('_')]
            for name in plan_names:
                exec(f'del {name}')

        
    """    
    # iterate over names list
    for name in names_list:
        # calling decorator with plan name
        @register_line_magic(name)
        # define generic line magic function
        def lmagic(line, magic_name = name):
            # concatenate '_plan' to name and line 
            line = magic_name + '_plan ' + line
            # create the bluesky command for the plan
            formatted_line = make_command(line)
            # evaluate the formatted line in the current scope
            ipython_ns = get_ipython().user_ns
            return eval('RE('+formatted_line+')', ipython_ns)
        
        
def get_imported_objects(path_to_file):
        """This method returns a list of the objects imported in the file self.path_to_file

        Returns:
            list: the imports found the in the file
        """        
        imported_objects = []
        
        # Parse the AST of the file
        with open(path_to_file, 'r') as file:
            # Tree of objects from import statements
            tree = ast.parse(file.read())
        
        # Traverse the AST nodes
        for node in ast.iter_child_nodes(tree):
            if isinstance(node, ast.ImportFrom):
                # Check if the import statement is not a relative import
                if node.module:
                    # Iterate over the imported names
                    for alias in node.names:
                        # if module is imported with alias
                        if alias.asname:
                            imported_objects.append(alias.asname)
                        else:
                            imported_objects.append(alias.name)
        return imported_objects
   
import re

def make_command(line):
    """
    Takes a string and writes it using correct bluesky syntax.

    Example:
        dscan [det1] motor1 -1 1 10
        becomes 
        dscan([det1], motor1, -1, 1, 10)

    Additionally, it automatically takes care of the case
    where the user has defined a list with detectors.

    Args:
        line (str): This is the ipython line for which an autogenerated
                    magic exists.

    Returns:
        str: A string representing a correct bluesky command.
    """        
    # Initialize the counter to enter the while loop
    count = 1

    # Start a loop to continuously check for consecutive spaces and reduce them to a single space
    while count > 0: 
        # Replace double spaces with a single space and update the count with the number of replacements made
        line, count = re.subn('  ', ' ', line)

    # If the specific pattern " [" is found, indicating a list is included
    if " [" in line:
        # Replace the first occurrence of ' [' with '([' to start the list in the correct format
        line = re.sub(' \[', '([', line)
        # Replace remaining spaces with commas to separate items within the list or tuple
        line = re.sub(' ', ',', line)
    else:
        # If no list is found, use the detectors in ps.plot_detector
        # Replace the first space with '(plot_detectors,' to include the detectors
        line = line.replace(" ", "(plot_detectors,", 1)
    
    # Replace remaining spaces with commas to separate items within the list or tuple
    line = line.replace(" ", ",")

    # Append a closing parenthesis to complete the tuple or list-like syntax
    line += ')'

    # Return the transformed line
    return line

    
def get_labeled_devices(user_ns=None, maxdepth=8):
    ''' Returns dict of labels mapped to devices with that label

        Parameters
        ----------
        user_ns : dict, optional
            The namespace to search on
            Default is to grab the namespace of the ipython shell.

        maxdepth: int, optional
            max recursion depth

        Returns
        -------
            A dictionary of (name, ophydobject) tuple indexed by device label.

        Examples
        --------
        Read devices labeled as motors:
            objs = get_labeled_devices()
            my_motors = objs['motors']
    '''    
    # could be set but lists are more common for users
    obj_list = collections.defaultdict(list)

    if maxdepth <= 0:
        warnings.warn("Recursion limit exceeded. Results will be truncated.")
        return obj_list

    if user_ns is None:
        from IPython import get_ipython
        user_ns = get_ipython().user_ns

    for key, obj in user_ns.items():
        # ignore objects beginning with "_"
        # (mainly for ipython stored objs from command line
        # return of commands)
        # also check its a subclass of desired classes
        
        if not key.startswith("_"):
            if hasattr(obj, '_ophyd_labels_'):
                # don't inherit parent labels
                labels = obj._ophyd_labels_
                for label in labels:
                    obj_list[label].append((key, obj))

                if is_parent(obj):
                    # Get direct children (not grandchildren).
                    try:
                        children = {k: getattr(obj, k)
                                # obj might be a Signal (no read_attrs).
                                for k in getattr(obj, 'read_attrs', [])
                                if '.' not in k}
                        # Recurse over all children.
                        for c_key, v in get_labeled_devices(
                                user_ns=children,
                                maxdepth=maxdepth-1).items():
                            items = [('.'.join([key, ot[0]]), ot[1]) for ot in v]
                            obj_list[c_key].extend(items)
                    except Exception as e:
                        if obj.name == 'pgm': # is this needed?
                            obj_list['pgm'].append(obj)
                        else:
                            print(f'########################################')
                            print(f'problem with obj {obj.name}')
                            print(f"the following Exception was raised: {e} ")

    # Convert from defaultdict to normal dict before returning.
    label_obj_dict = {k: sorted(v) for k, v in obj_list.items()}
    return label_obj_dict


def is_parent(dev):
    # return whether a node is a parent
    # should not have component_names, or if yes, should be empty
    # read_attrs needed to check it's an instance and not class itself
    return (not isinstance(dev, type) and getattr(dev, 'component_names', []))


def color_generator():
    while True:
        yield "black"
        yield "red"

def execute_magic(magic_string, user_ns=user_ns):
        """Executes a magic command given by the magic_string.

        This method strips the '%' prefix from the magic_string, transforms it into a command,
        and evaluates it within the context of the shell's user namespace.

        Args:
            magic_string (str): A string representing the magic command to execute. Expected to start with '%'.

        Returns:
            The result of the evaluated magic command.
        """
        if magic_string.startswith('%'):
            magic_string = magic_string[1:]  # Remove the '%' prefix from the magic command.
        
        cmd_string = make_command(magic_string)  # Transform the magic string into a command.
        cmd_string_RE = 'RE(' + cmd_string + ')'  # Format the command string for evaluation.
        
        return eval(cmd_string_RE, user_ns)  # Evaluate the command and return the result.


