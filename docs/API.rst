API
****

RunEngineBessy
===============

.. autoclass:: bessyii.RunEngine.RunEngineBessy.RunEngineBessy
   :members:


Elog
============

.. autoclass:: bessyii.eLog.ELogCallback
   :members:

.. autofunction:: bessyii.eLog.requestInvestigationName
.. autofunction:: bessyii.eLog.getSessionID
.. autofunction:: bessyii.eLog.writeToELog
.. autofunction:: bessyii.eLog.authenticate_session
.. autofunction:: bessyii.eLog.logout_session




