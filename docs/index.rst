.. bessyii documentation master file, created by
   sphinx-quickstart on Thu Oct 27 09:19:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bessyii's documentation!
===================================

A collection of tools and scripts useful for data collection and analysis at BESSY II

**Installation**

To install pull this repository and then run 

.. code:: python
   
   python3 -m pip install .

If you are working in a development and not a production environment use

.. code:: python

   python3 -m pip install -e .

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   API




